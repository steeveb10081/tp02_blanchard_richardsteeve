package fr.yahoo.steeveb1008.tp02_blanchard_richardsteeve;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText editTextNb1;
    private EditText editTextNb2;
    private Button buttonCompute;
    private final int COMPUTE_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    /**
     * Methods that allow to retrieve the instance of views in the layout
     */
    private void initViews() {
        editTextNb1 = findViewById(R.id.nb1);
        editTextNb2 = findViewById(R.id.nb2);
        buttonCompute = findViewById(R.id.compute);

        // desactivation du button
        buttonCompute.setEnabled(false);

        editTextNb1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(editTextNb1.getText().toString().equals("")  || editTextNb2.getText().toString().equals("")){
                        buttonCompute.setEnabled(false);
                    }else{
                        buttonCompute.setEnabled(true);
                    }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        editTextNb2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(editTextNb1.getText().toString().equals("")  || editTextNb2.getText().toString().equals("")){
                    buttonCompute.setEnabled(false);
                }else{
                    buttonCompute.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        // Intercept click on the compute button
        buttonCompute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textNb1 = editTextNb1.getText().toString();
                String textNb2 = editTextNb2.getText().toString();

                if(textNb1 == null || textNb2==null || textNb1.trim().equals("")  || textNb2.trim().equals("") ){
                    Toast.makeText(MainActivity.this, "Tous les champs sont obligatoire", Toast.LENGTH_SHORT).show();
                    if( textNb1 == null || textNb1.trim().equals("") ) {
                        editTextNb1.setError("Champs obligatoire") ;
                    }

                    if( textNb2 == null || textNb2.trim().equals("") ) {
                        editTextNb2.setError("Champs obligatoire") ;
                    }

                }else{
                    String texte_afiche = String.format("Tu veux calculer la somme de %s et %s -> %s et %s sont les valeurs des deux champs nb1 et nb2 respectivement",
                            textNb1, textNb2, textNb1, textNb2);
                    Toast.makeText(MainActivity.this, texte_afiche, Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this, ComputeActivity.class);
                    intent.putExtra("nb1", textNb1);
                    intent.putExtra("nb2", textNb2);


                    //startActivity(intent);
                    startActivityForResult(intent, COMPUTE_CODE);
                }



            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == COMPUTE_CODE && data != null) {
                String result = data.getStringExtra("result");
                Toast.makeText(this, "Result is " + result, Toast.LENGTH_SHORT).show();
            }
        }else{
            if(resultCode == ComputeActivity.RESULT_MINE){
                if (requestCode == COMPUTE_CODE ) {
                    String message_error = data.getStringExtra("message_error");
                    Toast.makeText(this, message_error, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


}